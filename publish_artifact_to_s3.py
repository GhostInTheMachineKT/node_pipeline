import os

os.system("apt-get update && \
   apt-get install -y python-dev && \
   curl -O https://bootstrap.pypa.io/get-pip.py && \
   python get-pip.py && \
   pip install awscli && \
   aws deploy push --application-name $APPLICATION_NAME --s3-location s3://$S3_BUCKET/$S3_BUCKET_FOLDER/$APPLICATION_NAME-$BITBUCKET_BUILD_NUMBER.tar --ignore-hidden-files")
